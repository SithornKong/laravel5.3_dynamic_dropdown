-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 02, 2017 at 06:14 AM
-- Server version: 5.6.17
-- PHP Version: 5.6.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `lara_dynamic_dropdown`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `productname` varchar(50) NOT NULL,
  `qty` int(11) NOT NULL,
  `price` float NOT NULL,
  `prod_cat_id` int(255) NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `productname`, `qty`, `price`, `prod_cat_id`, `created_at`, `updated_at`) VALUES
(1, 'CPU', 45, 567, 4, '2016-12-28 06:53:59', '2016-12-28 06:53:59'),
(2, 'Shirt (Male)', 3, 7658, 1, '2016-12-28 06:55:20', '2016-12-28 06:55:20'),
(7, 'Pant (Female)', 677, 4535, 1, '2016-12-28 06:56:31', '2016-12-28 06:56:31'),
(8, 'Keyboard', 3, 566, 4, '2016-12-28 06:56:31', '2016-12-28 06:56:31'),
(9, 'Bed', 78, 534, 3, '2016-12-28 06:57:01', '2016-12-28 06:57:01'),
(10, 'Soup', 63, 345, 2, '2016-12-28 06:57:23', '2016-12-28 06:57:23'),
(11, 'Skirt', 5436, 654, 1, '2016-12-28 06:58:28', '2016-12-28 06:58:28'),
(12, 'Table', 654, 654767, 3, '2016-12-28 06:58:28', '2016-12-28 06:58:28'),
(13, 'Burger', 524, 56, 2, '2016-12-28 06:58:54', '2016-12-28 06:58:54'),
(14, 'Chair', 46, 6546, 3, '2016-12-28 06:59:17', '2016-12-28 06:59:17'),
(15, 'Mouse', 5, 535, 4, '2016-12-28 07:00:22', '2016-12-28 07:00:22'),
(16, 'Monitor', 43, 5426, 4, '2016-12-28 07:00:22', '2016-12-28 07:00:22'),
(17, 'Vegetable', 5436, 7, 2, '2016-12-28 07:01:26', '2016-12-28 07:01:26'),
(18, 'Fish', 43, 546, 2, '2016-12-28 07:01:00', '2016-12-28 07:01:00');

-- --------------------------------------------------------

--
-- Table structure for table `product_cat`
--

CREATE TABLE IF NOT EXISTS `product_cat` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `product_cat_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `product_cat`
--

INSERT INTO `product_cat` (`id`, `product_cat_name`) VALUES
(1, 'Clothes'),
(2, 'Food'),
(3, 'Furniture'),
(4, 'Computer');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
